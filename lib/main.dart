import 'package:fContact/pages/home/home_contact.dart';
import 'package:fContact/pages/home/provider/add_contact_provider.dart';
import 'package:fContact/pages/home/provider/home_contact_provider.dart';
import 'package:fContact/pages/sign_in/presentation/sign_in_page.dart';
import 'package:fContact/pages/sign_in/provider/sign_in_provider.dart';
import 'package:fContact/state_manager.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:provider/provider.dart';

import 'core/hive_gen.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await initHive();

  runApp(
    MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (_) => AddContactProvider(),
        ),
        ChangeNotifierProvider(
          create: (_) => HomeContactProvider(),
        ),
        ChangeNotifierProvider(
          create: (_) => SignInProvider(),
        )
      ],
      child: const MyApp(),
    ),
  );
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: StateManager(
          state: signInProvider,
          builder: () {
            return Hive.box("isSignIn").get("isSign") == true
                ? HomeContacts()
                : SignInPage();
          }),
    );
  }
}
