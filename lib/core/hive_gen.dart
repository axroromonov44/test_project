import 'package:hive_flutter/hive_flutter.dart';

import '../pages/home/model/contact_model.dart';

Future<void> initHive() async {
  Hive.registerAdapter(ContactModelAdapter());
  await Hive.initFlutter();

  await Hive.openBox("User");

  await Hive.openBox('isSignIn');

  await Hive.openBox("ContactModel");

  return;
}
