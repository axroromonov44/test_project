import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../pages/home/provider/home_contact_provider.dart';


class CustomAppBar extends StatelessWidget with PreferredSizeWidget {
  String title;
  bool isHome;

  CustomAppBar({Key? key, required this.title, this.isHome = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      centerTitle: true,
      title: Text(title),
      actions: [
        if (isHome == true)
          IconButton(
              onPressed: () => homeContactProvider.logOut(context),
              icon: homeContactProvider.isLoading
                  ? const CupertinoActivityIndicator()
                  : const Icon(Icons.logout))
      ],
    );
  }

  @override
  Size get preferredSize => const Size.fromHeight(kToolbarHeight);
}
