import 'package:flutter/material.dart';

class CustomBtn extends StatelessWidget {
  Widget child;
  void Function()? onPressed;

  CustomBtn({Key? key, required this.child, this.onPressed}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      decoration: BoxDecoration(
          color: Colors.blue, borderRadius: BorderRadius.circular(12)),
      child: MaterialButton(
        onPressed: onPressed,
        child: child,
      ),
    );
  }
}
