import 'package:flutter/cupertino.dart';
import 'package:hive/hive.dart';

import '../../home/home_contact.dart';
import '../model/user_model.dart';

class SignInProvider extends ChangeNotifier {
  bool isLoading = false;
  String? errorText;
  late TextEditingController emailController, passwordController;

  void init() {
    emailController = TextEditingController();
    passwordController = TextEditingController();
  }

  void dispos() {
    emailController.dispose();
    passwordController.dispose();
  }

  List<UserModel> users = [
    UserModel(
      email: "test2023@gmail.com",
      password: "test2023",
    )
  ];

  Future<void> signIn(BuildContext context, UserModel userModel) async {
    if (emailController.text.isNotEmpty && passwordController.text.isNotEmpty) {
      if (userModel.email == users[0].email &&
          userModel.password == users[0].password) {
        loading(true);
        Future.delayed(const Duration(seconds: 2)).then((value) async {
          errorText = '';
          notifyListeners();
          loading(false);
          Navigator.pushReplacement(
              context, CupertinoPageRoute(builder: (_) => HomeContacts()));
          Hive.box("isSignIn").put("isSign", true);
        });
      } else {
        errorText = 'Login or password was entered incorrectly';
        notifyListeners();
      }
    } else {
      errorText = "Fields must be filled";
      notifyListeners();
    }
  }

  void loading(bool value) {
    isLoading = value;
    notifyListeners();
  }
}

SignInProvider signInProvider = SignInProvider();
