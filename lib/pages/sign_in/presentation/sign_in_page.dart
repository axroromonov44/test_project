import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:keyboard_dismisser/keyboard_dismisser.dart';

import '../../../core/components/custom_btn.dart';
import '../../../core/components/neu_textfield.dart';
import '../../../state_manager.dart';
import '../model/user_model.dart';
import '../provider/sign_in_provider.dart';

class SignInPage extends StatefulWidget {
  const SignInPage({Key? key}) : super(key: key);

  @override
  State<SignInPage> createState() => _SignInPageState();
}

class _SignInPageState extends State<SignInPage> {
  @override
  void initState() {
    signInProvider.init();
    super.initState();
  }

  @override
  void dispose() {
    signInProvider.dispos();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return StateManager(
        state: signInProvider,
        builder: () {
          return KeyboardDismisser(
            child: Scaffold(
              body: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    const Text(
                      "fContact",
                      style: TextStyle(
                        color: Colors.blue,
                        fontWeight: FontWeight.bold,
                        fontSize: 24,
                      ),
                    ),
                    const SizedBox(height: 30),
                    NeuTextfield(
                      hintText: "E-mail",
                      controller: signInProvider.emailController,
                      onChanged: (email) {
                        if (email.isNotEmpty) {
                          signInProvider.errorText = '';
                          setState(() {});
                        }
                      },
                    ),
                    const SizedBox(height: 10),
                    NeuTextfield(
                      hintText: "Password",
                      controller: signInProvider.passwordController,
                      onChanged: (password) {
                        if (password.isNotEmpty) {
                          signInProvider.errorText = '';
                          setState(() {});
                        }
                      },
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    if (signInProvider.errorText != null)
                      Text(
                        signInProvider.errorText!,
                        style: const TextStyle(color: Colors.red),
                      ),
                    const SizedBox(height: 60),
                    CustomBtn(
                        onPressed: () async {
                          var userModel = UserModel(
                            password: signInProvider.passwordController.text,
                            email: signInProvider.emailController.text,
                          );
                          signInProvider.signIn(context, userModel);
                        },
                        child: signInProvider.isLoading
                            ?  CupertinoActivityIndicator()
                            :  Text(
                                'Sign In',
                                style: TextStyle(
                                  color: Colors.black,
                                ),
                              ))
                  ],
                ),
              ),
            ),
          );
        });
  }
}
