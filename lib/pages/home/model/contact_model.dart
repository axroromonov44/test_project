import 'package:hive/hive.dart';

part 'contact_model.g.dart';

@HiveType(typeId: 1)
class ContactModel {
  @HiveField(0)
  String? id;
  @HiveField(1)
  String phoneNumber;
  @HiveField(2)
  String fullName;
  @HiveField(3)
  String email;

  ContactModel({
    required this.id,
    required this.phoneNumber,
    required this.email,
    required this.fullName,
  });
}
