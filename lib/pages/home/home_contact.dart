import 'package:fContact/pages/home/provider/home_contact_provider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:keyboard_dismisser/keyboard_dismisser.dart';

import '../../core/components/custom_appbar.dart';
import '../../state_manager.dart';
import 'presentation/add_new_contact_page.dart';
import 'presentation/contact_list.dart';
import 'presentation/search_field.dart';
import 'presentation/search_response_list.dart';

class HomeContacts extends StatefulWidget {
  const HomeContacts({Key? key}) : super(key: key);

  @override
  State<HomeContacts> createState() => _HomeContactsState();
}

class _HomeContactsState extends State<HomeContacts> {

  @override
  Widget build(BuildContext context) {
    return KeyboardDismisser(
      child: StateManager(
        state: homeContactProvider,
        builder: () {
          return Scaffold(
            backgroundColor: Colors.white,
            appBar: CustomAppBar(
              title: 'fContact',
              isHome: true,
            ),
            body: SingleChildScrollView(
              child: Column(
                children: [
                  SearchField(),
                  Stack(
                    children: [
                      ContactList(),
                      if (homeContactProvider.searchResponseList.isNotEmpty)
                        SearchResponseList()
                      else if (homeContactProvider.notFound == true ||
                          homeContactProvider.notFoundContact != '')
                        Container(
                          height: 600,
                          width: double.infinity,
                          color: Colors.white,
                          alignment: Alignment.center,
                          child: Center(
                            child: Text(homeContactProvider.notFoundContact,style: TextStyle(fontSize: 16,fontWeight: FontWeight.normal),),
                          ),
                        )
                    ],
                  )
                ],
              ),
            ),
            floatingActionButton: FloatingActionButton(
              onPressed: () {
                Navigator.push(
                  context,
                  CupertinoPageRoute(
                    builder: (_) => AddNewContactPage(),
                  ),
                );
              },
              child: const Icon(
                Icons.add,
              ),
            ),
          );
        },
      ),
    );
  }
}
