import 'package:flutter/cupertino.dart';
import 'package:hive_flutter/hive_flutter.dart';

import '../../sign_in/presentation/sign_in_page.dart';

class HomeContactProvider extends ChangeNotifier {
  bool isLoading = false;
  bool isClick = false;
  List searchList = [];
  List searchResponseList = [];
  String notFoundContact = '';
  bool notFound = false;

  Future<void> searchResponse(String text) async {
    if (text.isNotEmpty) {
      for (var i in searchList) {
        if (i.phoneNumber
            .replaceAll(" ", "")
            .toLowerCase()
            .contains(text.toLowerCase())) {
          if (searchResponseList.contains(i)) {
            searchResponseList.clear();
          }
          searchResponseList.add(i);
        } else if (i.fullName
            .replaceAll(" ", "")
            .toLowerCase()
            .contains(text.toLowerCase())) {
          if (searchResponseList.contains(i)) {
            searchResponseList.clear();
          }
          searchResponseList.add(i);
        } else if (i.email
            .replaceAll(" ", "")
            .toLowerCase()
            .contains(text.toLowerCase())) {
          if (searchResponseList.contains(i)) {
            searchResponseList.clear();
          }
          searchResponseList.add(i);
        } else {
          if (searchResponseList.contains(i)) {
            searchResponseList.clear();
          }
          notFoundContact = 'Contact "$text " was not found';
          notFound = true;
          notifyListeners();
        }
      }
    } else {
      notFound = false;
      notFoundContact = '';
      searchResponseList.clear();
      notifyListeners();
    }
  }

  Future<void> filter() async {
    if (isClick) {
      searchList = searchList.reversed.toList();
      notifyListeners();
    } else {
      searchList = searchList;
      notifyListeners();
    }
  }

  void logOut(BuildContext context) {
    loading(true);
    Future.delayed(const Duration(seconds: 2)).then((value) {
      Hive.box("User").clear();
      Hive.box("isSignIn").clear();
      Hive.box("ContactModel").clear();
      homeContactProvider.searchList.clear();
      homeContactProvider.searchResponseList.clear();
      Navigator.pushReplacement(
          context, CupertinoPageRoute(builder: (_) => SignInPage()));
      notifyListeners();
      loading(false);
    });
  }

  void loading(bool value) {
    isLoading = value;
    notifyListeners();
  }
}

HomeContactProvider homeContactProvider = HomeContactProvider();
