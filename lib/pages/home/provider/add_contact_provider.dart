import 'package:flutter/cupertino.dart';
import 'package:hive/hive.dart';
import 'package:uuid/uuid.dart';
import 'package:url_launcher/url_launcher.dart' as UrlLauncher;

import '../model/contact_model.dart';

class AddContactProvider extends ChangeNotifier {
  bool isLoading = false;
  String? errorText;
  late TextEditingController phoneController, nameController, emailController;
  late ContactModel contactModel;
  final _contactBox = Hive.box('ContactModel');

  void init() {
    contactModel = ContactModel(
      id: null,
      phoneNumber: phoneController.text,
      email: emailController.text,
      fullName: nameController.text,
    );
  }

  void dispos() {
    phoneController.dispose();
    nameController.dispose();
    emailController.dispose();
    errorText = null;
  }

  void addOrUpdateContact(
      BuildContext context, ContactModel? oldContact, int? index) async {
    if (phoneController.text.isNotEmpty &&
        nameController.text.isNotEmpty &&
        emailController.text.isNotEmpty) {
      loading(true);
      if (oldContact != null) {
        final updateContact = ContactModel(
          id: oldContact.id,
          phoneNumber: phoneController.text,
          email: emailController.text,
          fullName: nameController.text,
        );
        Future.delayed(const Duration(seconds: 2)).then((value) {
          _contactBox.putAt(index!, updateContact);
          loading(false);
          notifyListeners();
          Navigator.pop(context);
        });
      } else {
        var uuid = const Uuid();
        contactModel.id = uuid.v1();
        errorText = " ";
        Future.delayed(const Duration(seconds: 2)).then((value) {
          _contactBox.add(contactModel);
          loading(false);
          notifyListeners();
          Navigator.pop(context);
        });
      }
    } else {
      errorText = "All fields must be filled";
      notifyListeners();
    }
  }

  void deleteItem(int index) {
    _contactBox.deleteAt(index);
    notifyListeners();
  }

  Future<void> callPhone(String phone) async {
    final Uri url = Uri(
      scheme: 'tel',
      path: phone,
    );
    await UrlLauncher.launchUrl(url);
  }

  void loading(bool value) {
    isLoading = value;
    notifyListeners();
  }
}

AddContactProvider addContactProvider = AddContactProvider();
