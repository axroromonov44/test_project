import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:keyboard_dismisser/keyboard_dismisser.dart';
import '../../../core/components/custom_appbar.dart';
import '../../../core/components/custom_btn.dart';
import '../../../core/components/neu_textfield.dart';
import '../../../state_manager.dart';
import '../../sign_in/provider/sign_in_provider.dart';
import '../model/contact_model.dart';
import '../provider/add_contact_provider.dart';

class AddNewContactPage extends StatefulWidget {
  ContactModel? contactModel;
  int? index;

  AddNewContactPage({Key? key, this.contactModel, this.index})
      : super(key: key);

  @override
  State<AddNewContactPage> createState() => _AddNewContactPageState();
}

class _AddNewContactPageState extends State<AddNewContactPage> {
  @override
  void initState() {
    addContactProvider.phoneController =
        TextEditingController(text: widget.contactModel?.phoneNumber);
    addContactProvider.nameController =
        TextEditingController(text: widget.contactModel?.fullName);
    addContactProvider.emailController =
        TextEditingController(text: widget.contactModel?.email);
    addContactProvider.init();
    super.initState();
  }

  @override
  void dispose() {
    addContactProvider.dispos();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return StateManager(
      state: addContactProvider,
      builder: () {
        return KeyboardDismisser(
          child: StateManager(
              state: signInProvider,
              builder: () {
                return Scaffold(
                  appBar: CustomAppBar(
                    title: 'Add New Contact',
                  ),
                  body: Padding(
                    padding: const EdgeInsets.all(12.0),
                    child: Column(
                      children: [
                        NeuTextfield(
                          prefix: Icon(Icons.phone),
                          hintText: "+998",
                          keyboardType: TextInputType.phone,
                          mask: "+998 ## ### ## ##",
                          controller: addContactProvider.phoneController,
                          textCapitalization: TextCapitalization.sentences,
                          onChanged: (phone) {
                            addContactProvider.contactModel.phoneNumber = phone;
                          },
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        NeuTextfield(
                          prefix: Icon(Icons.person),
                          keyboardType: TextInputType.text,
                          hintText: "Full name",
                          textCapitalization: TextCapitalization.sentences,
                          controller: addContactProvider.nameController,
                          onChanged: (name) {
                            addContactProvider.contactModel.fullName = name;
                          },
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        NeuTextfield(
                          prefix: Icon(
                            Icons.alternate_email,
                          ),
                          keyboardType: TextInputType.emailAddress,
                          hintText: "E-mail",
                          controller: addContactProvider.emailController,
                          onChanged: (email) {
                            addContactProvider.contactModel.email = email;
                          },
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        if (addContactProvider.errorText != null)
                          Text(
                            addContactProvider.errorText!,
                            style: const TextStyle(
                              color: Colors.red,
                            ),
                          ),
                        const Spacer(),
                        CustomBtn(
                          onPressed: () async {
                            addContactProvider.addOrUpdateContact(
                                context, widget.contactModel, widget.index);
                          },
                          child: addContactProvider.isLoading
                              ? CupertinoActivityIndicator()
                              : Text("add contact"),
                        ),
                        const SizedBox(
                          height: 40,
                        )
                      ],
                    ),
                  ),
                );
              }),
        );
      },
    );
  }
}
