import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../../core/components/neu_textfield.dart';
import '../../../state_manager.dart';
import '../provider/home_contact_provider.dart';

class SearchField extends StatefulWidget {
  SearchField({Key? key}) : super(key: key);

  @override
  State<SearchField> createState() => _SearchFieldState();
}

class _SearchFieldState extends State<SearchField> {
  @override
  Widget build(BuildContext context) {
    return StateManager(
        state: homeContactProvider,
        builder: () {
          return Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              children: [
                Expanded(
                  flex: 7,
                  child: NeuTextfield(
                    onChanged: (searchText) {
                      homeContactProvider.searchResponse(searchText);
                    },
                    keyboardType: TextInputType.text,
                    hintText: "Searching....",
                  ),
                ),
                Expanded(
                    child: IconButton(
                        onPressed: () {
                          homeContactProvider.isClick =
                              !homeContactProvider.isClick;
                          homeContactProvider.filter();
                        },
                        icon: homeContactProvider.isClick
                            ? Icon(CupertinoIcons.sort_up)
                            : Icon(CupertinoIcons.sort_down)))
              ],
            ),
          );
        });
  }
}
