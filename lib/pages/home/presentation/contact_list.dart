import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:hive_flutter/hive_flutter.dart' as hive;
import '../provider/add_contact_provider.dart';
import '../provider/home_contact_provider.dart';
import 'add_new_contact_page.dart';

class ContactList extends StatelessWidget {
  const ContactList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder<hive.Box>(
      valueListenable: hive.Hive.box("ContactModel").listenable(),
      builder: (context, box, child) {
        homeContactProvider.searchList = box.values.toList();
        List searchList = [];
        searchList.addAll(homeContactProvider.searchList);
        if (!homeContactProvider.isClick) {
          searchList = searchList;
        } else {
          searchList = searchList.reversed.toList();
        }
        return searchList.isEmpty
            ? const Padding(
                padding: EdgeInsets.symmetric(vertical: 300),
                child: Text(
                  'No Contacts',
                    style: TextStyle(fontSize: 16)
                ),
              )
            : ListView.separated(
                separatorBuilder: (BuildContext context, int index) =>
                    const SizedBox(
                      height: 10,
                    ),
                padding: const EdgeInsets.all(16.0),
                itemCount: searchList.length,
                physics: const NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                itemBuilder: (BuildContext context, int index) {
                  return GestureDetector(
                    onTap: () {
                      addContactProvider
                          .callPhone(searchList[index].phoneNumber);
                    },
                    child: Slidable(
                      key: UniqueKey(),
                      endActionPane: ActionPane(
                        motion: const ScrollMotion(),
                        children: [
                          SlidableAction(
                            borderRadius: BorderRadius.circular(12),
                            onPressed: (context) {
                              Navigator.push(
                                  context,
                                  CupertinoPageRoute(
                                      builder: (_) => AddNewContactPage(
                                            contactModel: searchList[index],
                                            index: index,
                                          )));
                            },
                            backgroundColor: Colors.blue,
                            foregroundColor: Colors.white,
                            icon: Icons.edit,
                            label: 'Edit',
                          ),
                          SlidableAction(
                            borderRadius: BorderRadius.circular(12),
                            onPressed: (context) =>
                                addContactProvider.deleteItem(index),
                            backgroundColor: const Color(0xFFFE4A49),
                            foregroundColor: Colors.white,
                            icon: Icons.delete,
                            label: 'Delete',
                          ),
                        ],
                      ),
                      child: Card(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(15.0),
                        ),
                        child: Container(
                          padding: const EdgeInsets.all(12.0),
                          width: double.infinity,
                          child: Column(
                            children: [
                              Row(
                                children: [
                                  const Text(
                                    "Phone:",
                                    style: TextStyle(
                                        fontWeight: FontWeight.normal),
                                  ),
                                  const SizedBox(
                                    width: 20,
                                  ),
                                  Text(searchList[index].phoneNumber,
                                      style: const TextStyle(
                                          fontWeight: FontWeight.bold)),
                                ],
                              ),
                              const SizedBox(
                                height: 5,
                              ),
                              Row(
                                children: [
                                  const Text("Name:",
                                      style: TextStyle(
                                          fontWeight: FontWeight.normal)),
                                  const SizedBox(
                                    width: 20,
                                  ),
                                  Text(searchList[index].fullName,
                                      style: const TextStyle(
                                          fontWeight: FontWeight.bold)),
                                ],
                              ),
                              const SizedBox(
                                height: 5,
                              ),
                              Row(
                                children: [
                                  const Text("E-mail:",
                                      style: TextStyle(
                                          fontWeight: FontWeight.normal)),
                                  const SizedBox(
                                    width: 20,
                                  ),
                                  Text(searchList[index].email,
                                      style: const TextStyle(
                                          fontWeight: FontWeight.bold)),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  );
                });
      },
    );
  }
}
