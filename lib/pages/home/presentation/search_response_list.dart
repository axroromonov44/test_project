import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import '../../../state_manager.dart';
import '../provider/home_contact_provider.dart';

class SearchResponseList extends StatelessWidget {
  const SearchResponseList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StateManager(
      state: homeContactProvider,
      builder: () {
        return Container(
          height: 900,
          width: double.infinity,
          child: Material(
            color: Colors.white,
            child: ListView.separated(
                separatorBuilder: (BuildContext context, int index) =>
                const SizedBox(
                  height: 10,
                ),
                padding: const EdgeInsets.all(16.0),
                itemCount: homeContactProvider.searchResponseList.length,
                physics: const NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                itemBuilder: (BuildContext context, int index) {
                  return Slidable(
                    key: UniqueKey(),
                    child: Card(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(15.0),
                      ),
                      child: Container(
                        padding: const EdgeInsets.all(12.0),
                        width: double.infinity,
                        child: Column(
                          children: [
                            Row(
                              children: [
                                const Text(
                                  "Phone:",
                                  style:
                                  TextStyle(fontWeight: FontWeight.normal),
                                ),
                                const SizedBox(
                                  width: 20,
                                ),
                                Text(homeContactProvider.searchResponseList[index].phoneNumber,
                                    style: const TextStyle(
                                        fontWeight: FontWeight.bold)),
                              ],
                            ),
                            const SizedBox(
                              height: 5,
                            ),
                            Row(
                              children: [
                                const Text("Name:",
                                    style: TextStyle(
                                        fontWeight: FontWeight.normal)),
                                const SizedBox(
                                  width: 20,
                                ),
                                Text(homeContactProvider.searchResponseList[index].fullName,
                                    style: const TextStyle(
                                        fontWeight: FontWeight.bold)),
                              ],
                            ),
                            const SizedBox(
                              height: 5,
                            ),
                            Row(
                              children: [
                                const Text("E-mail:",
                                    style: TextStyle(
                                        fontWeight: FontWeight.normal)),
                                const SizedBox(
                                  width: 20,
                                ),
                                Text(homeContactProvider.searchResponseList[index].email,
                                    style: const TextStyle(
                                        fontWeight: FontWeight.bold)),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                  );
                }),
          ),
        );
      }
    );
  }
}
