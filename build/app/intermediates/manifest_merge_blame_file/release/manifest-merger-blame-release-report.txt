1<?xml version="1.0" encoding="utf-8"?>
2<manifest xmlns:android="http://schemas.android.com/apk/res/android"
3    package="com.example.new_contact"
4    android:versionCode="1"
5    android:versionName="1.0.0" >
6
7    <uses-sdk
8        android:minSdkVersion="16"
8-->/Users/axroromonov/dev/test_project/android/app/src/main/AndroidManifest.xml
9        android:targetSdkVersion="31" />
9-->/Users/axroromonov/dev/test_project/android/app/src/main/AndroidManifest.xml
10
11    <queries>
11-->/Users/axroromonov/dev/test_project/android/app/src/main/AndroidManifest.xml:3:5-14:15
12
13        <!-- If your app checks for SMS support -->
14        <intent>
14-->/Users/axroromonov/dev/test_project/android/app/src/main/AndroidManifest.xml:5:9-8:18
15            <action android:name="android.intent.action.VIEW" />
15-->/Users/axroromonov/dev/test_project/android/app/src/main/AndroidManifest.xml:6:13-65
15-->/Users/axroromonov/dev/test_project/android/app/src/main/AndroidManifest.xml:6:21-62
16
17            <data android:scheme="sms" />
17-->/Users/axroromonov/dev/test_project/android/app/src/main/AndroidManifest.xml:7:13-42
17-->/Users/axroromonov/dev/test_project/android/app/src/main/AndroidManifest.xml:7:19-39
18        </intent>
19        <!-- If your app checks for call support -->
20        <intent>
20-->/Users/axroromonov/dev/test_project/android/app/src/main/AndroidManifest.xml:10:9-13:18
21            <action android:name="android.intent.action.VIEW" />
21-->/Users/axroromonov/dev/test_project/android/app/src/main/AndroidManifest.xml:6:13-65
21-->/Users/axroromonov/dev/test_project/android/app/src/main/AndroidManifest.xml:6:21-62
22
23            <data android:scheme="tel" />
23-->/Users/axroromonov/dev/test_project/android/app/src/main/AndroidManifest.xml:7:13-42
23-->/Users/axroromonov/dev/test_project/android/app/src/main/AndroidManifest.xml:7:19-39
24        </intent>
25    </queries>
26
27    <application
28        android:name="android.app.Application"
28-->/Users/axroromonov/dev/test_project/android/app/src/main/AndroidManifest.xml:17:9-42
29        android:appComponentFactory="androidx.core.app.CoreComponentFactory"
29-->[androidx.core:core:1.6.0] /Users/axroromonov/.gradle/caches/transforms-3/4ca28c4a71705f8a687c69dda140a277/transformed/core-1.6.0/AndroidManifest.xml:24:18-86
30        android:icon="@mipmap/ic_launcher"
30-->/Users/axroromonov/dev/test_project/android/app/src/main/AndroidManifest.xml:18:9-43
31        android:label="fContact" >
31-->/Users/axroromonov/dev/test_project/android/app/src/main/AndroidManifest.xml:16:9-33
32        <activity
32-->/Users/axroromonov/dev/test_project/android/app/src/main/AndroidManifest.xml:19:9-39:20
33            android:name="com.example.new_contact.MainActivity"
33-->/Users/axroromonov/dev/test_project/android/app/src/main/AndroidManifest.xml:20:13-41
34            android:configChanges="orientation|keyboardHidden|keyboard|screenSize|smallestScreenSize|locale|layoutDirection|fontScale|screenLayout|density|uiMode"
34-->/Users/axroromonov/dev/test_project/android/app/src/main/AndroidManifest.xml:24:13-163
35            android:exported="true"
35-->/Users/axroromonov/dev/test_project/android/app/src/main/AndroidManifest.xml:21:13-36
36            android:hardwareAccelerated="true"
36-->/Users/axroromonov/dev/test_project/android/app/src/main/AndroidManifest.xml:25:13-47
37            android:launchMode="singleTop"
37-->/Users/axroromonov/dev/test_project/android/app/src/main/AndroidManifest.xml:22:13-43
38            android:theme="@style/LaunchTheme"
38-->/Users/axroromonov/dev/test_project/android/app/src/main/AndroidManifest.xml:23:13-47
39            android:windowSoftInputMode="adjustResize" >
39-->/Users/axroromonov/dev/test_project/android/app/src/main/AndroidManifest.xml:26:13-55
40
41            <!--
42                 Specifies an Android theme to apply to this Activity as soon as
43                 the Android process has started. This theme is visible to the user
44                 while the Flutter UI initializes. After that, this theme continues
45                 to determine the Window background behind the Flutter UI.
46            -->
47            <meta-data
47-->/Users/axroromonov/dev/test_project/android/app/src/main/AndroidManifest.xml:31:13-34:17
48                android:name="io.flutter.embedding.android.NormalTheme"
48-->/Users/axroromonov/dev/test_project/android/app/src/main/AndroidManifest.xml:32:15-70
49                android:resource="@style/NormalTheme" />
49-->/Users/axroromonov/dev/test_project/android/app/src/main/AndroidManifest.xml:33:15-52
50
51            <intent-filter>
51-->/Users/axroromonov/dev/test_project/android/app/src/main/AndroidManifest.xml:35:13-38:29
52                <action android:name="android.intent.action.MAIN" />
52-->/Users/axroromonov/dev/test_project/android/app/src/main/AndroidManifest.xml:36:17-68
52-->/Users/axroromonov/dev/test_project/android/app/src/main/AndroidManifest.xml:36:25-66
53
54                <category android:name="android.intent.category.LAUNCHER" />
54-->/Users/axroromonov/dev/test_project/android/app/src/main/AndroidManifest.xml:37:17-76
54-->/Users/axroromonov/dev/test_project/android/app/src/main/AndroidManifest.xml:37:27-74
55            </intent-filter>
56        </activity>
57        <!--
58             Don't delete the meta-data below.
59             This is used by the Flutter tool to generate GeneratedPluginRegistrant.java
60        -->
61        <meta-data
61-->/Users/axroromonov/dev/test_project/android/app/src/main/AndroidManifest.xml:42:9-44:33
62            android:name="flutterEmbedding"
62-->/Users/axroromonov/dev/test_project/android/app/src/main/AndroidManifest.xml:43:13-44
63            android:value="2" />
63-->/Users/axroromonov/dev/test_project/android/app/src/main/AndroidManifest.xml:44:13-30
64
65        <activity
65-->[:url_launcher_android] /Users/axroromonov/dev/test_project/build/url_launcher_android/intermediates/merged_manifest/release/AndroidManifest.xml:10:9-13:74
66            android:name="io.flutter.plugins.urllauncher.WebViewActivity"
66-->[:url_launcher_android] /Users/axroromonov/dev/test_project/build/url_launcher_android/intermediates/merged_manifest/release/AndroidManifest.xml:11:13-74
67            android:exported="false"
67-->[:url_launcher_android] /Users/axroromonov/dev/test_project/build/url_launcher_android/intermediates/merged_manifest/release/AndroidManifest.xml:12:13-37
68            android:theme="@android:style/Theme.NoTitleBar.Fullscreen" />
68-->[:url_launcher_android] /Users/axroromonov/dev/test_project/build/url_launcher_android/intermediates/merged_manifest/release/AndroidManifest.xml:13:13-71
69
70        <uses-library
70-->[androidx.window:window:1.0.0-beta04] /Users/axroromonov/.gradle/caches/transforms-3/dee59bb480020f42d53c2ea15e4501f7/transformed/jetified-window-1.0.0-beta04/AndroidManifest.xml:25:9-27:40
71            android:name="androidx.window.extensions"
71-->[androidx.window:window:1.0.0-beta04] /Users/axroromonov/.gradle/caches/transforms-3/dee59bb480020f42d53c2ea15e4501f7/transformed/jetified-window-1.0.0-beta04/AndroidManifest.xml:26:13-54
72            android:required="false" />
72-->[androidx.window:window:1.0.0-beta04] /Users/axroromonov/.gradle/caches/transforms-3/dee59bb480020f42d53c2ea15e4501f7/transformed/jetified-window-1.0.0-beta04/AndroidManifest.xml:27:13-37
73        <uses-library
73-->[androidx.window:window:1.0.0-beta04] /Users/axroromonov/.gradle/caches/transforms-3/dee59bb480020f42d53c2ea15e4501f7/transformed/jetified-window-1.0.0-beta04/AndroidManifest.xml:28:9-30:40
74            android:name="androidx.window.sidecar"
74-->[androidx.window:window:1.0.0-beta04] /Users/axroromonov/.gradle/caches/transforms-3/dee59bb480020f42d53c2ea15e4501f7/transformed/jetified-window-1.0.0-beta04/AndroidManifest.xml:29:13-51
75            android:required="false" />
75-->[androidx.window:window:1.0.0-beta04] /Users/axroromonov/.gradle/caches/transforms-3/dee59bb480020f42d53c2ea15e4501f7/transformed/jetified-window-1.0.0-beta04/AndroidManifest.xml:30:13-37
76    </application>
77
78</manifest>
